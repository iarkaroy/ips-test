<?php

namespace App\Http\Controllers;

use App\Http\Helpers\InfusionsoftHelper;
use Illuminate\Http\Request;
use Response;
use Exception;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Module;
use App\Tag;

class ApiController extends Controller
{
    public $infusionsoftHelper;

    public function __construct(InfusionsoftHelper $infusionsoftHelper) {
        $this->infusionsoftHelper = $infusionsoftHelper;
    }

    public function moduleReminderAssigner(Request $request) {

        $success = true;
        $message = '';

        try {

            // Check request method
            // Allow only post
            $this->checkRequestMethod($request);

            // Check required param contact_email
            // Also fail if not valid email
            $this->checkRequiredParam($request);

            // Making email lowercase for consistency
            $email = strtolower( $request->contact_email );

            // Get user details from infusionsoft
            $userData = $this->infusionsoftHelper->getContact($email);

            // Invalid user
            if( $userData === false ) {
                throw new Exception('Invalid user.');
            }

            // User does not have any course
            if( !$userData['_Products'] ) {
                throw new Exception('User has not subscribed to any course.');
            }

            // Get subscribed courses
            $courses = array_map('trim', explode(',', $userData['_Products']));

            // User id in infusionsoft, needed for addTag
            $infusionsoftUserId = $userData['Id'];

            $user = User::where('email', $email)->first();

            // Default tag, if no conditions match i.e. user completed the last modules of all courses
            $tagName = 'Module reminders completed';

            foreach( $courses as $course ) {

                // Get last completed module of the current course
                $lastCompletedModule = $user->completed_modules()
                                                ->where('course_key', $course)
                                                ->orderBy('modules.name', 'desc')
                                                ->first();

                // Get total number of modules in the current course,
                // For now, it is 7 but trying to make it more dynamic
                $totalModulesInCourse = Module::where('course_key', $course)->count();


                if( !$lastCompletedModule ) {
                    // User has not started the course yet
                    // So need to tag the first module of this course
                    $tagName = 'Start ' . strtoupper($course) . ' Module 1 Reminders';
                    break;
                } else {
                    // User has completed some of the modules
                    // Need to find out the correct next module
                    $moduleName = $lastCompletedModule->name;

                    // Assuming the module names will always have the number at the end
                    preg_match('/\s+(\d+)$/', $moduleName, $matches);
                    $moduleNumber = $matches[1];

                    // If user has completed the last module, we do not need to do anything
                    // as the loop will check the next course automatically
                    // We only need to check if any previous module is completed
                    if( $moduleNumber < $totalModulesInCourse ) {
                        $tagName = 'Start ' . strtoupper($course) . ' Module ' . ($moduleNumber + 1) . ' Reminders';
                        break;
                    }
                }
            }

            $tag = Tag::where('name', $tagName)->first();

            // Call to infusionsoft for tagging
            $added = $this->infusionsoftHelper->addTag($infusionsoftUserId, $tag->id);

            if( !$added ) {
                throw new Exception('Failed to add tag to contact.');
            }

            $message = $tag->name;

        } catch( Exception $e ) {
            $success = false;
            $message = $e->getMessage();
        }

        $response = [
            'success' => $success,
            'message' => $message
        ];
        $status = $success ? 200 : 500;
        return response()->json($response, $status);
    }

    private function checkRequestMethod($request)
    {
        if( !$request->isMethod('POST') ) {
            throw new Exception('Only post method allowed.');
        }
    }

    private function checkRequiredParam($request)
    {
        $validator = Validator::make($request->all(), [
            'contact_email' => 'required|email'
        ]);

        if( $validator->fails() ) {
            throw new Exception( $validator->messages()->first() );
        }
    }

    public function exampleCustomer(){

        $infusionsoft = new InfusionsoftHelper();

        $uniqid = uniqid();

        $infusionsoft->createContact([
            'Email' => $uniqid.'@test.com',
            "_Products" => 'ipa,iea'
        ]);

        $user = User::create([
            'name' => 'Test ' . $uniqid,
            'email' => $uniqid.'@test.com',
            'password' => bcrypt($uniqid)
        ]);

        // attach IPA M1-3 & M5
        $user->completed_modules()->attach(Module::where('course_key', 'ipa')->limit(3)->get());
        $user->completed_modules()->attach(Module::where('name', 'IPA Module 5')->first());


        return $user;
    }
}
