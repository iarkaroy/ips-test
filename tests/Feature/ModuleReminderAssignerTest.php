<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Helpers\InfusionsoftHelper;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ModuleReminderAssignerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Clean test environment
     */
    public function tearDown() {
        \Mockery::close();
    }
    
    /**
     * Setup Test Environment
     */
    public function setUp() {
        parent::setUp();
        $this->artisan("db:seed");
    }

    public function testAllowOnlyPostMethod()
    {
        $httpVerbs = ['get', 'put', 'patch', 'delete'];

        foreach ($httpVerbs as $httpVerb) {
            $response = $this->$httpVerb('/api/module_reminder_assigner');
            $response
                ->assertStatus(500)
                ->assertJson([
                    'success' => false,
                    'message' => 'Only post method allowed.'
                ]);
        }
    }

    public function testRequiredParam()
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->json('POST', '/api/module_reminder_assigner', []);
        $response
            ->assertStatus(500)
            ->assertJson([
                'success' => false,
                'message' => 'The contact email field is required.'
            ]);
    }

    public function testValidateParam()
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->json('POST', '/api/module_reminder_assigner', ['contact_email' => 'invalid@email']);
        $response
            ->assertStatus(500)
            ->assertJson([
                'success' => false,
                'message' => 'The contact email must be a valid email address.'
            ]);
    }

    public function testValidUser()
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->json('POST', '/api/module_reminder_assigner', ['contact_email' => 'invaliduser@infusionsoft.com']);
        $response
            ->assertStatus(500)
            ->assertJson([
                'success' => false,
                'message' => 'Invalid user.'
            ]);
    }

    public function testUserWithNoCourse()
    {
        $user = factory(\App\User::class)->create();
        $infusionsoftHelper = $this->mock(InfusionsoftHelper::class);
        $infusionsoftHelper->shouldReceive('getContact')
            ->with($user->email)
            ->andReturn(['Id' => 1234, 'Email' => $user->email, '_Products' => '']);
        
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->json('POST', '/api/module_reminder_assigner', ['contact_email' => $user->email]);
        $response
            ->assertStatus(500)
            ->assertJson([
                'success' => false,
                'message' => 'User has not subscribed to any course.'
            ]);
    }

    public function testFirstModuleIfNoModuleCompleted()
    {
        $user = factory(\App\User::class)->create();
        $infusionsoftHelper = $this->mock(InfusionsoftHelper::class);
        $infusionsoftHelper->shouldReceive('getContact')
            ->with($user->email)
            ->andReturn(['Id' => 1234, 'Email' => $user->email, '_Products' => 'ipa,iea']);
        $infusionsoftHelper->shouldReceive('addTag')->once()->andReturn(true);
        
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->json('POST', '/api/module_reminder_assigner', ['contact_email' => $user->email]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
                'message' => 'Start IPA Module 1 Reminders'
            ]);
    }

    public function testCorrectNextModuleIfSomeCompletedInFirstCourse()
    {
        $user = factory(\App\User::class)->create();
        $user->completed_modules()->attach(\App\Module::where('name', 'IPA Module 4')->first());
        $infusionsoftHelper = $this->mock(InfusionsoftHelper::class);
        $infusionsoftHelper->shouldReceive('getContact')
            ->with($user->email)
            ->andReturn(['Id' => 1234, 'Email' => $user->email, '_Products' => 'ipa,iea']);
        $infusionsoftHelper->shouldReceive('addTag')->once()->andReturn(true);
        
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->json('POST', '/api/module_reminder_assigner', ['contact_email' => $user->email]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
                'message' => 'Start IPA Module 5 Reminders'
            ]);
    }

    public function testNextCourseIfLastModuleCompletedInFirstCourse()
    {
        $user = factory(\App\User::class)->create();
        $user->completed_modules()->attach(\App\Module::where('name', 'IPA Module 7')->first());
        $infusionsoftHelper = $this->mock(InfusionsoftHelper::class);
        $infusionsoftHelper->shouldReceive('getContact')
            ->with($user->email)
            ->andReturn(['Id' => 1234, 'Email' => $user->email, '_Products' => 'ipa,iea']);
        $infusionsoftHelper->shouldReceive('addTag')->once()->andReturn(true);
        
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->json('POST', '/api/module_reminder_assigner', ['contact_email' => $user->email]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
                'message' => 'Start IEA Module 1 Reminders'
            ]);
    }

    public function testCorrectNextModuleInNextCourseIfLastModuleCompletedInPreviousCourse()
    {
        $user = factory(\App\User::class)->create();
        $user->completed_modules()->attach(\App\Module::where('name', 'IPA Module 7')->first());
        $user->completed_modules()->attach(\App\Module::where('name', 'IEA Module 6')->first());
        $infusionsoftHelper = $this->mock(InfusionsoftHelper::class);
        $infusionsoftHelper->shouldReceive('getContact')
            ->with($user->email)
            ->andReturn(['Id' => 1234, 'Email' => $user->email, '_Products' => 'ipa,iea']);
        $infusionsoftHelper->shouldReceive('addTag')->once()->andReturn(true);
        
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->json('POST', '/api/module_reminder_assigner', ['contact_email' => $user->email]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
                'message' => 'Start IEA Module 7 Reminders'
            ]);
    }

    public function testCorrectNextModuleWithThreeCourses()
    {
        $user = factory(\App\User::class)->create();
        $user->completed_modules()->attach(\App\Module::where('name', 'IPA Module 7')->first());
        $user->completed_modules()->attach(\App\Module::where('name', 'IEA Module 7')->first());
        $user->completed_modules()->attach(\App\Module::where('name', 'IAA Module 5')->first());
        $infusionsoftHelper = $this->mock(InfusionsoftHelper::class);
        $infusionsoftHelper->shouldReceive('getContact')
            ->with($user->email)
            ->andReturn(['Id' => 1234, 'Email' => $user->email, '_Products' => 'ipa,iea,iaa']);
        $infusionsoftHelper->shouldReceive('addTag')->once()->andReturn(true);
        
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->json('POST', '/api/module_reminder_assigner', ['contact_email' => $user->email]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
                'message' => 'Start IAA Module 6 Reminders'
            ]);
    }

    public function testCompleteIfLastModulesCompleted()
    {
        $user = factory(\App\User::class)->create();
        $user->completed_modules()->attach(\App\Module::where('name', 'IPA Module 7')->first());
        $user->completed_modules()->attach(\App\Module::where('name', 'IEA Module 7')->first());
        $user->completed_modules()->attach(\App\Module::where('name', 'IAA Module 7')->first());
        $infusionsoftHelper = $this->mock(InfusionsoftHelper::class);
        $infusionsoftHelper->shouldReceive('getContact')
            ->with($user->email)
            ->andReturn(['Id' => 1234, 'Email' => $user->email, '_Products' => 'ipa,iea,iaa']);
        $infusionsoftHelper->shouldReceive('addTag')->once()->andReturn(true);
        
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->json('POST', '/api/module_reminder_assigner', ['contact_email' => $user->email]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
                'message' => 'Module reminders completed'
            ]);
    }

    public function testFailedToAddTag()
    {
        $user = factory(\App\User::class)->create();
        $user->completed_modules()->attach(\App\Module::where('name', 'IPA Module 7')->first());
        $user->completed_modules()->attach(\App\Module::where('name', 'IEA Module 7')->first());
        $user->completed_modules()->attach(\App\Module::where('name', 'IAA Module 7')->first());
        $infusionsoftHelper = $this->mock(InfusionsoftHelper::class);
        $infusionsoftHelper->shouldReceive('getContact')
            ->with($user->email)
            ->andReturn(['Id' => 1234, 'Email' => $user->email, '_Products' => 'ipa,iea,iaa']);
        $infusionsoftHelper->shouldReceive('addTag')->once()->andReturn(false);
        
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->json('POST', '/api/module_reminder_assigner', ['contact_email' => $user->email]);
        $response
            ->assertStatus(500)
            ->assertJson([
                'success' => false,
                'message' => 'Failed to add tag to contact.'
            ]);
    }

    public function mock($class)
    {
        $mock = \Mockery::mock($class);
        $this->app->instance($class, $mock);
        return $mock;
    }

}
