<?php

use Illuminate\Database\Seeder;
use App\Http\Helpers\InfusionsoftHelper;
use App\Tag;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $infusionsoft = new InfusionsoftHelper();
        $infusionsoftTags = json_decode( json_encode( $infusionsoft->getAllTags() ) );
        foreach( $infusionsoftTags as $tag ) {
            Tag::insert([
                'id' => $tag->id,
                'name' => $tag->name,
                'description' => $tag->description
            ]);
        }
    }
}
